# Mòdul M3 de programació

Aquest es un repositori de programacio amb llenguatge C  

## Índex
- ### [El tutorial amb MarkDown](#tutorial-amb-markdown)
- ### [Programació en Java](#programacio-amb-java)

# Tutorial amb MarkDown
---

[YT](https://www.youtube.com) 

![MarkDown](imatges/baixa.png)


<div style="text-align: right">

[Tornar a l'índex...](#índex)

<div>

## Programació JAVA
---

## introducció
---
* primavera
* estiu
    * aaaa
        * eeee
* tardor
* hivern
1. primer
1. segon
1. tercer<br><br>
**aixó es una frase amb negreta**

_està en cursiva_

### sub sub titol<br><br>

#### adagsgkndhklsmhd<br><br>

##### aaaaaa<br><br>

###### eeeeeee<br><br>
```js 
//aquest es un comentari.
a=5;
var pi=32;
``` 
hola que &nbsp;  tal 
&nbsp;&nbsp;
## Taules
--- 
| NOM | Cognoms | Adreça | Edat |
| --- | ---| --- | --- |
| Àlex | Rubio | NS | 88 |
| Àlex | Rubio | NS | 22 |
| Àlex | Rubio | NS | 47 |







